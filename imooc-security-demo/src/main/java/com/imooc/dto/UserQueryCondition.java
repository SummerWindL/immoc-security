package com.imooc.dto;
/**
 *@date 2018年5月25日-下午11:06:06
 *@author fu yanliang
 *@action(作用)
 *@instruction
 */
public class UserQueryCondition {

	
	private String username;
	
	private int age;
	
	private int ageTo;
	
	private String xxx;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getAgeTo() {
		return ageTo;
	}

	public void setAgeTo(int ageTo) {
		this.ageTo = ageTo;
	}

	public String getXxx() {
		return xxx;
	}

	public void setXxx(String xxx) {
		this.xxx = xxx;
	}
	
	
}
